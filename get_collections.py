import argparse
import pymongo
from bson.json_util import dumps

class Downloader:
    def __init__(self, con_str, db_name):
        try:
            self.mongo = pymongo.MongoClient(con_str)
            self.db = self.mongo[db_name]
            self.mongo.server_info()
        except Exception as e:
            print(e)
            print("Error while connecting to data base")
    
    def get_json(self, path):
        collections = self.db.list_collection_names()
        for collection_str in collections:
            collection = self.db[collection_str]
            cursor = collection.find()
            with open(f'{path}/'+f'{collection_str}'+'.json', 'w') as file:
                list_cur = list(cursor)
                n_json = dumps(list_cur,indent = 2)
                file.write(n_json)  


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Get collections and save them in json file")
    parser.add_argument("-cs", "--con_str", default="", type=str,
                        help="Connection string")
    parser.add_argument("-dn", "--db_name", default="", type=str,
                        help="Connection string")
    parser.add_argument("-p", "--path", default="",
                        type=str, help="path of directory to save download json files")
    args = parser.parse_args()

    downloader = Downloader(args.con_str, args.db_name)
    downloader.get_json(args.path)

